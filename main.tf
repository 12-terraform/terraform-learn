provider "aws" {
  region = "ca-central-1"
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name: var.cidr_blocks[0].name
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.cidr_blocks[1].cidr_block
  availability_zone = var.avail_zone
    tags = {
    Name: var.cidr_blocks[1].name
  }
}

# data "aws_vpc" "existing_vpc" {
#     default = true
# }

# resource "aws_subnet" "dev-subnet-b" {
#     vpc_id = data.aws_vpc.existing_vpc.id
#     cidr_block = var.subnet_cidr_block
#     availability_zone = "ca-central-1a"
#     tags = {
#       Name: "dev_subnet_b"
#     }
  
# }

# output "vpc_id" {
#   value = aws_vpc.dev-vpc.id
# }

# output "subenet_b_id" {
#   value = aws_subnet.dev-subnet-b
# }